'''
Created on Jun 29, 2012

@author: xding
'''


from shortcuts import execute_sql
import datasets
import gtfsfeeds


def prepare_db():
    datasets.init_dataset_schema()
    execute_sql("commit")


def create_dataset(dataset_name):
    if dataset_name == "gtfs": return
    datasets.create_tables(dataset_name, "MYISAM")
    execute_sql("commit")


def drop_dataset(dataset_name):
    if dataset_name == "gtfs": return
    datasets.drop_tables(dataset_name)
    execute_sql("commit")


def build_union(dataset_list = None):
    datasets.union(dataset_list)
    execute_sql("commit")


def import_data(dataset_name, path, delimiter=',', quotechar='"'):
    if dataset_name == "gtfs": return
    gtfsfeeds.importCSV(dataset_name, path, delimiter, quotechar)
    execute_sql("commit")
