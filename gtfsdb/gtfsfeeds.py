'''
Created on Jun 29, 2012

@author: xding
'''

from sqllib import gtfs_table_list
from shortcuts import execute_select, execute_sql, execute_insert, log_info
import csv
import os


def importCSV(dataset_name, dataset_path, delimiter=',', quotechar='"'):
    log_info("Import data " + dataset_name)
    for data_file in os.listdir(dataset_path):
        table = data_file.split(".")[0]
        if table not in gtfs_table_list:
            continue
        table_full_name = "_".join([dataset_name, table])
        execute_sql("DELETE FROM %s" % table_full_name)
        csvReader = csv.DictReader(open(os.path.join(dataset_path, data_file), 'r'), delimiter=delimiter, quotechar=quotechar)
        columns = csvReader.fieldnames
        for row in csvReader:
            if csvReader.line_num % 10000 == 2:
                log_info(" - %s %d" % (table_full_name, csvReader.line_num - 2))
            inputs = [(i, row.get(i)) for i in columns]
            try:
                execute_insert(table_full_name, inputs)
            except:
                pass
    
    fix_agency_id(dataset_name)
    #end import


def fix_agency_id(dataset_name):
    agency = execute_select("SELECT agency_id FROM %s_agency" % dataset_name)
    if len(agency) == 1:
        agency_id = agency[0][0]
        if agency_id == "" or agency_id is None:
            execute_sql("UPDATE %s_agency SET agency_id='0'" % dataset_name, True)
            agency_id = "0"
        execute_sql("UPDATE %s_routes SET agency_id='%s'" % (dataset_name, agency_id), True)
