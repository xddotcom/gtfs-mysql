'''
Created on 2012-7-30

@author: Xindong
'''


from shortcuts import execute_select, log_info


def load_station_graph():
    graph = {int(item[0]): dict() for item in 
             execute_select("SELECT node_id, latitude, longitude FROM graph_nodes")}
    results = execute_select("SELECT dep_node, arr_node, min(cost) FROM graph_arcs GROUP BY dep_node, arr_node")
    for item in results:
        dep_node, arr_node = int(item[0]), int(item[1])
        cost = int(item[2])
        graph[dep_node][arr_node] = cost
        graph[arr_node][dep_node] = cost
    core = [node for node in graph if len(graph[node]) != 2]
    results = execute_select("SELECT node1, node2, cost FROM graph_nearby_nodes")
    for item in results:
        node1, node2 = int(item[0]), int(item[1])
        cost = int(item[2])
        graph[node1][node2] = cost #walking in the simple graph doesn't take time
    return graph, core


def load_query_graph(route_set = None):
    '''@return: dict: a time dependent graph (sub graph introduced by certain routes)
        3 type of nodes:
            - the pysical station (we dont really put a graph node for it)
            - a node for each route
            - a entry/exit node for origin, destin and walking
        3 types of valued arcs:
            - route arc
            - transfers between route nodes on the same physical station
            - walking links from exit nodes to entry nodes
        non-valued arc:
            - arcs from route nodes to exit nodes (start walking or terminate)
            - arcs from entry nodes to route nodes (stop walking and waiting for bus)
    '''
    log_info("Load query graph, %s pre-fetched routes" % ["without","with"][route_set is not None])
    
    graph = {}
    arcnodes_out = {}
    arcnodes_in = {}
    
    def add_arc(dep, arr, value):
        graph.setdefault(dep, dict()).setdefault(arr, value)
        graph.setdefault(arr, dict())
    
    def append_arcnode_out(node, arcnode, route):
        arcnodes_out.setdefault(node, list()).append((arcnode, route))
        arcnodes_in.setdefault(node, list())
    
    def append_arcnode_in(node, arcnode, route):
        arcnodes_in.setdefault(node, list()).append((arcnode, route))
        arcnodes_out.setdefault(node, list())
    
    if route_set is not None:
        where_clause = (" ".join(["OR route = %d" % route for route in route_set]))
        results = execute_select("""
            SELECT dep_node, arr_node, arc_id, route
            FROM graph_arcs
            WHERE 0 %s
        """ % where_clause)
    else:
        results = execute_select("SELECT dep_node, arr_node, arc_id, route FROM graph_arcs")
    
    for item in results:
        dep_node, arr_node, arc_id, route = int(item[0]), int(item[1]), int(item[2]), int(item[3])
        
        dep_arcnode = dep_node, arc_id
        arr_arcnode = arr_node, arc_id
        
        add_arc(dep_arcnode, arr_arcnode, value = {"route_arc": arc_id})
        
        add_arc((dep_node,"enter"), dep_arcnode, value = {})
        append_arcnode_out(dep_node, dep_arcnode, route)
        
        add_arc(arr_arcnode, (arr_node,"exit"), value = {})
        append_arcnode_in(arr_node, arr_arcnode, route)
        
    """ for each node_arc on the same node, add transfer links """
    for node in arcnodes_in:
        #enumerate all the nodes
        for arcnode_in, route_in in arcnodes_in[node]:
            for arcnode_out, route_out in arcnodes_out[node]:
                transfer = [1, 0][route_in == route_out]
                add_arc(arcnode_in, arcnode_out, value={"transfer_arc": transfer})
    
    results = execute_select("SELECT node1, node2, cost FROM graph_nearby_nodes")
    log_info("%d walking links loaded for query group" % len(results))
    for item in results:
        walking_from = int(item[0]), "exit"
        walking_to = int(item[1]), "enter"
        cost = int(item[2])
        if walking_from in graph and walking_to in graph:
            add_arc(walking_from, walking_to, value = {"walking_arc": cost})
    
    log_info("Graph size: %d" % len(graph))
    
    return graph


if __name__ == "__main__":
    print("Problem?")

