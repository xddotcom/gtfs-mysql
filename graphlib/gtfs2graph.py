'''
Created on Jul 6, 2012

@author: xding
'''

from sqllib import graph_table_list, table_sql
from graphlib import globalmap
from shortcuts import execute_sql, execute_select, execute_insert, log_info


def init_graph_schema():
    log_info("Init graph tables")
    for table in graph_table_list:
        execute_sql("DROP TABLE IF EXISTS " + table, print_sql=False)
        sql = table_sql[table] % "MyISAM"
        execute_sql(sql, print_sql=False)
    execute_sql("commit")


def build_graph_nodes():
    log_info("build graph nodes (a node each stop)")
    
    stops = execute_select("SELECT dataset_id, stop_id, stop_lat, stop_lon FROM gtfs_stops")
    stop_to_node = {}
    for stop in stops:
        dataset_id, stop_id, stop_lat, stop_lon = stop[:4]
        precision = 1e+5
        ''' we store coordinates as integer: 120.375292 -> 12037529 '''
        latitude  = int(round(float(stop_lat) * precision))
        longitude = int(round(float(stop_lon) * precision))
        tile_id = globalmap.calculate_tile_id(latitude, longitude)
        inputs = [("latitude", latitude), ("longitude", longitude), ("tile_id", tile_id), 
                  ("gtfs_dataset", dataset_id), ("gtfs_stop", stop_id)]
        node_id = execute_insert("graph_nodes", inputs)
        stop_to_node[(dataset_id, stop_id)] = node_id
    execute_sql("commit")
    return stop_to_node


def get_graph_route_id(gtfs_dataset, gtfs_route, node_list, graph_route_tree, route_arc_sequence):
    LEAF = "___not_a_node_id_but_a_route_id___"
    p = graph_route_tree.setdefault((gtfs_dataset, gtfs_route), dict())
    for node in node_list:
        if node not in p:
            p[node] = dict()
        p = p[node]
    if LEAF in p:
        """route exists"""
        return p[LEAF]
    else:
        """new route found"""
        inputs = [("gtfs_dataset", gtfs_dataset), ("gtfs_route", gtfs_route)]
        graph_route_id = execute_insert("graph_routes", inputs)
        for node in node_list:
            inputs = [("node_id", node), ("route_id", graph_route_id)]
            execute_insert("graph_route_incidence", inputs)
        route_arc_sequence[graph_route_id] = sequence = []
        for (dep_node, arr_node) in zip(node_list[:-1], node_list[1:]):
            inputs = [("dep_node", dep_node), ("arr_node", arr_node), ("route", graph_route_id), ("cost", 0)]
            arc_id = execute_insert("graph_arcs", inputs)
            sequence.append(arc_id)
        execute_sql("commit")
        p[LEAF] = graph_route_id
        return graph_route_id


def build_graph():
    stop2node = build_graph_nodes()
    
    graph_route_tree = dict()
    route_arc_sequence = dict()
    
    trips = execute_select("SELECT dataset_id, route_id, trip_id FROM gtfs_trips ORDER BY dataset_id, route_id")
    log_info("build graph for %d trips" % len(trips))
    for (i, trip) in enumerate(trips):
        if i % 1000 == 0:
            log_info(" - graph %d" % i)
        dataset_id, gtfs_route_id, trip_id = trip[:3]
        stop_times = execute_select("""
            SELECT dataset_id, stop_id, TIME_TO_SEC(arrival_time), TIME_TO_SEC(departure_time)
            FROM gtfs_stop_times WHERE dataset_id = %d AND trip_id = '%s'
            ORDER BY stop_sequence
        """ % (dataset_id, trip_id))
        node_list = [stop2node[(item[0], item[1])] for item in stop_times]
        if len(stop_times) >= 2:
            time_list = [(dep_item[3], arr_item[2]) for (dep_item, arr_item) in zip(stop_times[:-1], stop_times[1:])]
            graph_route_id = get_graph_route_id(dataset_id, gtfs_route_id, node_list, graph_route_tree, route_arc_sequence)
            for ((dep_time, arr_time), arc_id) in zip(time_list, route_arc_sequence[graph_route_id]):
                inputs = [("arc_id", arc_id), ("dep_time", dep_time), ("arr_time", arr_time), ("days", 0xFFFFFFF),]
                execute_insert("graph_timetable", inputs)
            execute_sql("commit")
    execute_sql("""
        UPDATE graph_arcs AS A JOIN graph_timetable AS T ON A.arc_id = T.arc_id 
        SET A.cost = T.arr_time - T.dep_time
    """, True)
    execute_sql("commit")

