
import gexf
import gtfs2graph
import graph_connector
import graph_hierarchy
import graph_coupling
import globalmap
from shortcuts import execute_sql, execute_select, log_info


def precomputation():
#    gtfs2graph.init_graph_schema()
#    gtfs2graph.build_graph()

#    graph_coupling.build_walking_links()
#    graph_coupling.build_adjacent_routes()
    
#    graph_hierarchy.select_hubs()
    graph_hierarchy.build_shortcuts()
    

if __name__ == '__main__':
    precomputation()

