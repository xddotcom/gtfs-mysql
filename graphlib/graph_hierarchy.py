'''
Created on 2012-8-2

@author: Xindong
'''

from shortcuts import log_info, execute_select, execute_sql, execute_insert
from algorithms import shortestpath


def load_graph():
    ''' return an undirected station graph '''
    graph = {int(item[0]): dict() for item in 
             execute_select("SELECT node_id, latitude, longitude FROM graph_nodes")}
    results = execute_select("SELECT dep_node, arr_node, min(cost) FROM graph_arcs GROUP BY dep_node, arr_node")
    for item in results:
        dep_node, arr_node = int(item[0]), int(item[1])
        cost = int(item[2])
        graph[dep_node][arr_node] = cost
        graph[arr_node][dep_node] = cost
    core = [node for node in graph if len(graph[node]) != 2]
    results = execute_select("SELECT node1, node2, cost FROM graph_nearby_nodes")
    for item in results:
        node1, node2 = int(item[0]), int(item[1])
        cost = int(item[2])
        graph[node1][node2] = cost #walking in the simple graph doesn't take time
    return graph, core


def build_shortcuts():
    execute_sql("DELETE FROM graph_shortcuts")
    execute_sql("commit")
    graph, _core = load_graph()
    hub_nodes = {int(item[0]) for item in
                 execute_select("SELECT node_id FROM graph_nodes WHERE ishub = 1")}
    inf = 0xFFFFFFF
    shortcuts = {}
    for i, hub in enumerate(hub_nodes):
        dist = shortestpath.sssp_tree(graph, hub, blackholes = hub_nodes)
        size = 0
        for node, cost in dist.iteritems():
            if cost < inf:
                size += 1
                shortcuts[(hub, node)] = cost
                shortcuts[(node, hub)] = cost
        log_info("sssp tree %d, size %d" % (i, size))
    log_info("%d shortcuts to be inserted" % len(shortcuts))
    for (dep, arr), cost in shortcuts.iteritems():
        inputs = [("dep_node", dep), ("arr_node", arr), ("cost", cost)]
        execute_insert("graph_shortcuts", inputs)
    execute_sql("commit")


def select_hubs():
    execute_sql("UPDATE graph_nodes SET ishub=0")
    execute_sql("commit")
    
    graph, core = load_graph()
    """suppose graph is connected!!!!!!"""
    graph_size = len(graph)
    max_num_of_paths = 3 * len(graph) / 2
    log_info("connect graph size %d, around %d paths to cover" % (graph_size, max_num_of_paths))
    
    path_list = []
    search_space = set()
    
    for x in core:
        for y in core:
            if x != y and x not in graph[y] and y not in graph[x]:
                search_space.add((x, y))
    log_info("search space size %s" % len(search_space))
    
    while len(search_space) > 0 and max_num_of_paths > 0:
        max_num_of_paths -= 1
        if max_num_of_paths % 100 == 0:
            log_info("%d paths left" % max_num_of_paths)
        (dep, arr) = search_space.pop()
        path, _cost = shortestpath.dijkstra(graph, dep, arr)
        path_list.append(path)
        for k in range(2, len(path)):
            for (x, y) in zip(path[:-k], path[k:]):
                search_space -= {(x, y)}
    
    """ recalculate incident paths, now we remove the source and the destination from each path
        and only interested by the first 1/2 of in the set
    """
    path_list = sorted(path_list, key = lambda x : len(x), reverse = True)
    #path_list = path_list[:len(path_list)/2+1]
    incident_paths = {node: set() for node in graph}
    for pathid, path in enumerate(path_list):
        #for node in path[1:-1]:
        for node in path:
            incident_paths[node].add(pathid)
    
    #find hubs convering half of the paths found
    hub_nodes = set()
    while True:
        hub = max(incident_paths, key = lambda x : len(incident_paths[x]))
        if len(incident_paths[hub]) == 0:
            break
        hub_nodes.add(hub)
        paths_to_remove = list(incident_paths[hub])
        for pathid in paths_to_remove:
            #for node in incident_paths:
            for node in path_list[pathid]:
                incident_paths[node] -= {pathid}
    log_info("%d hub nodes found" % len(hub_nodes))
    
    #record_hubs
    for node in hub_nodes:
        execute_sql("UPDATE graph_nodes SET ishub=1 WHERE node_id=%d" % node)
    execute_sql("commit")


if __name__ == '__main__':
    print("problem?")

