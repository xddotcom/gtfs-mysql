'''
Created on 2012-8-5

@author: DXD.Spirits
'''

"""
How to mesure the performance ?
    Execution Time
    Arcs/Nodes visited
    SQL queries sent
"""


import random, time
from algorithms import shortestpath
from shortcuts import log_info, execute_select
import graph_connector

def get_next_time(arc_id, cur_time, dep_node = None, arr_node = None, route = None):
    results = execute_select("""
        SELECT dep_time, arr_time FROM graph_timetable
        WHERE arc_id = %d AND dep_time >= %d
        ORDER BY dep_time LIMIT 1
    """ % (arc_id, cur_time))
    if results is not None and results:
        return int(results[0][0]), int(results[0][1])
    else:
        return None


INIT = [3600*10, 0, 0]
#INF = [3600*15, 300*5, 5]
#INIT = [0, 0, 0]
INF = [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF]


def cost_function(labelval, arcval):
    ''' labelval = [arrivaltime, walkingtime, changes]
    '''
    
    def value(arr_time, walking_time, transfer):
        if arr_time > INF[0] or walking_time > INF[1] or transfer > INF[2]:
            return inf()
        else:
            return [arr_time, walking_time, transfer]
    
    def inf():
        return [0xFFFFFFFF] * 3
    
    if "route_arc" in arcval:
        cur_time = labelval[0]
        arc_id = arcval["route_arc"]
        next_time = get_next_time(arc_id, cur_time)
        if next_time == None:
            return inf()
        else:
            _dep_time, arr_time = next_time
            return value(arr_time, labelval[1], labelval[2])
    elif "transfer_arc" in arcval:
        transfer = arcval["transfer_arc"]
        return value(labelval[0], labelval[1], labelval[2] + transfer)
    elif "walking_arc" in arcval:
        walking_time = arcval["walking_arc"]
        return value(labelval[0] + walking_time, labelval[1] + walking_time, labelval[2] + 1)
    else:
        return value(labelval[0], labelval[1], labelval[2])


def search_2_legs(origin, destin):

    results = execute_select("SELECT node2 FROM graph_nearby_nodes WHERE node1=%d" % origin)
    origins = {int(item[0]) for item in results} | {origin}
    
    results = execute_select("SELECT node1 FROM graph_nearby_nodes WHERE node2=%d " % destin)
    destins = {int(item[0]) for item in results} | {destin}
    
    ############################################################################################
    
    where_clause = " OR ".join(["node_id = %d" % node for node in origins])
    results = execute_select("SELECT route_id FROM graph_route_incidence WHERE %s" % where_clause)
    origin_routes = {int(item[0]) for item in results}
    
    where_clause = " OR ".join(["node_id = %d" % node for node in destins])
    results = execute_select("SELECT route_id FROM graph_route_incidence WHERE %s" % where_clause)
    destin_routes = {int(item[0]) for item in results}
    
    ############################################################################################
    
    route_set = origin_routes | destin_routes
    log_info("%d routes pre fetched" % len(route_set))
    
    return route_set


def search_3_legs(origin, destin):

    results = execute_select("SELECT node2 FROM graph_nearby_nodes WHERE node1=%d" % origin)
    origins = {int(item[0]) for item in results} | {origin}
    
    results = execute_select("SELECT node1 FROM graph_nearby_nodes WHERE node2=%d " % destin)
    destins = {int(item[0]) for item in results} | {destin}
    
    ############################################################################################
    
    where_clause = " OR ".join(["node_id = %d" % node for node in origins])
    results = execute_select("SELECT route_id FROM graph_route_incidence WHERE %s" % where_clause)
    origin_routes = {int(item[0]) for item in results}
    
    where_clause = " OR ".join(["node_id = %d" % node for node in destins])
    results = execute_select("SELECT route_id FROM graph_route_incidence WHERE %s" % where_clause)
    destin_routes = {int(item[0]) for item in results}
    
    ############################################################################################
    
    where_clause = " OR ".join(["route1 = %d" % route for route in origin_routes])
    results = execute_select("SELECT route2 FROM graph_adjacent_routes WHERE %s" % where_clause)
    origin_route_adjs = {int(item[0]) for item in results}
    
    where_clause = " OR ".join(["route2 = %d" % route for route in destin_routes])
    results = execute_select("SELECT route1 FROM graph_adjacent_routes WHERE %s" % where_clause)
    destin_route_adjs = {int(item[0]) for item in results}
    
    ############################################################################################
    
    route_set = origin_routes | destin_routes | (origin_route_adjs & destin_route_adjs)
    log_info("%d routes pre fetched" % len(route_set))
    
    return route_set


def profile_search_with_hubs(origin, destin):
    results = execute_select("""
        SELECT DISTINCT A.arr_node, A.cost + B.cost
        FROM graph_shortcuts AS A
        INNER JOIN graph_shortcuts AS B ON A.arr_node=B.dep_node
        WHERE A.dep_node=%d AND B.arr_node=%d
    """ % (origin, destin))
    hubs = [(int(item[0]), int(item[1])) for item in results]
    hub = min(hubs, key=lambda x: x[1])[0]
    route_set = search_3_legs(origin, hub) | search_3_legs(hub, destin)
    return route_set


def time_search(graph, origin, destin):
    origin = (origin, "enter")
    destin = (destin, "exit")
    path_list = []
    if len(graph) > 0 and origin in graph and destin in graph:
        path_list, _node_sequence = shortestpath.martins(graph, origin, destin, 
                                                         costfunc = cost_function, K = 3, 
                                                         init = INIT, inf = INF)
        for cost, path in path_list:
            arrival = "%02d:%02d:%02d" % (cost[0] / 3600, cost[0] % 3600 / 60, cost[0] % 60)
            log_info("A%s, W%03d, T%d: %s" % (arrival, cost[1], cost[2], path))
    return path_list


def search_path(origin, destin, heuristic = True):
    log_info("Travel Start: %d -> %d | Heuristic? %s" % (origin, destin, heuristic))
    if heuristic:
        log_info("search 2 legs")
        route_set = search_2_legs(origin, destin)
        graph = graph_connector.load_query_graph(route_set)
        path_list = time_search(graph, origin, destin)
        if path_list:
            return len(graph), len(path_list)
        
        log_info("search 3 legs")
        route_set = search_3_legs(origin, destin)
        graph = graph_connector.load_query_graph(route_set)
        path_list = time_search(graph, origin, destin)
        if path_list:
            return len(graph), len(path_list)
        
        log_info("search hubs")
        route_set = profile_search_with_hubs(origin, destin)
        graph = graph_connector.load_query_graph(route_set)
        path_list = time_search(graph, origin, destin)
        return len(graph), len(path_list)
    else:
        return 0, 0
        graph = graph_connector.load_query_graph()
        path_list = time_search(graph, origin, destin)
        return len(graph), len(path_list)
    log_info("Travel End.")


def random_test():
    #Travel Start: 7599 -> 8398 | Heuristic? True
    
    results = execute_select("SELECT count(*) FROM graph_nodes")
    nodesize = results[0][0]
    
    g1 = g2 = 0
    p1 = p2 = 0.0
    t1 = t2 = 0.0
    N = 10
    
    for _iiiii in range(N):
        log_info("------------------------------------------------------------------------------------------------------")
        origin = random.randint(1, nodesize)
        destin = random.randint(1, nodesize)
        
        start = time.time()
        graph_size, path_size = search_path(origin, destin, True)
        end = time.time()
        g1 += graph_size
        p1 += path_size
        t1 += (end - start)
        
        start = time.time()
        graph_size, path_size = search_path(origin, destin, False)
        end = time.time()
        g2 += graph_size
        p2 += path_size
        t2 += (end - start)
        log_info("------------------------------------------------------------------------------------------------------")
    
    log_info("network_size  %d" % nodesize)
    log_info("graph_size: %d  solution_size: %.3f  running time: %.3f" % (g1 / N, p1 / N, t1 / N))
    log_info("graph_size: %d  solution_size: %.3f  running time: %.3f" % (g2 / N, p2 / N, t2 / N))

if __name__ == "__main__":
    random_test()

