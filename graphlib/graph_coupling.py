'''
Created on 2012-8-2

@author: DXD.Spirits
'''

from shortcuts import log_info, execute_insert, execute_sql, execute_select
import globalmap


AVG_TRANSFER_TIME = 300 #seconds
MAX_TRANSFER_TIME = 1200 #seconds
USUAL_WALKING_DIS = 300 #m


def build_walking_links():
    execute_sql("DELETE FROM graph_nearby_nodes")
    execute_sql("commit")
    results = execute_select("SELECT node_id, latitude, longitude FROM graph_nodes")
    coordinate = {int(node_id): (int(latitude), int(longitude)) for node_id, latitude, longitude in results}
    log_info("Build walking links for %d stops" % len(coordinate))
    
    transfer_arcs = {}
    for i, (node, (lat, lon)) in enumerate(coordinate.iteritems()):
        if i % 1000 == 0:
            log_info(" - stop %d" % i)
        
        radius = USUAL_WALKING_DIS
        walking_time = AVG_TRANSFER_TIME
        neighbour_list = globalmap.find_neighbours(lat, lon, radius)
        while len(neighbour_list) <= 1 and walking_time <= MAX_TRANSFER_TIME:
            radius = radius << 1
            walking_time = walking_time << 1
            neighbour_list = globalmap.find_neighbours(lat, lon, radius)
        
        for neighbour in neighbour_list:
            transfer_arcs[(node, neighbour)] = walking_time
            transfer_arcs[(neighbour, node)] = walking_time
    
    log_info("%d transfer arcs found" % len(transfer_arcs)) 
    for (node1, node2), walking_time in transfer_arcs.iteritems():
        inputs = [("node1", node1), ("node2", node2), ("cost", walking_time)]
        execute_insert("graph_nearby_nodes", inputs)
    execute_sql("commit")
    
    return transfer_arcs


def build_adjacent_routes():
    execute_sql("DELETE FROM graph_adjacent_routes")
    execute_sql("commit")
    results = execute_select("""
        SELECT DISTINCT A.route_id, B.route_id 
        FROM graph_nearby_nodes as X
        JOIN graph_route_incidence AS A ON A.node_id=X.node1
        JOIN graph_route_incidence AS B ON B.node_id=X.node2
    """)
    log_info("%d pairs of adjacent routes to be inserted" % len(results))
    for i, item in enumerate(results):
        if i % 10000 == 0:
            log_info(i)
        route1, route2 = int(item[0]), int(item[1])
        inputs = [("route1", route1), ("route2", route2)]
        execute_insert("graph_adjacent_routes", inputs)
    execute_sql("commit")
    log_info("%d pairs of adjacent routes inserted" % len(results))


############################# heuristic #############################


def get_arr_times(arc_id):
    results = execute_select("""
        SELECT arr_time FROM graph_timetable WHERE arc_id=%d ORDER BY arr_time
    """ % arc_id)
    arr_times = [int(arr_time[0]) for arr_time in results]
    return arr_times


def get_dep_times(arc_id):
    results = execute_select("""
        SELECT dep_time FROM graph_timetable WHERE arc_id=%d ORDER BY dep_time
    """ % arc_id)
    dep_times = [int(dep_time[0]) for dep_time in results]
    return dep_times


def adjacent(arr_times, dep_times):
    gap = 3600
    iarr = idep = 0
    while iarr < len(arr_times) and idep < len(dep_times):
        if arr_times[iarr] < dep_times[idep] < arr_times[iarr] + gap:
            return True
        elif dep_times[idep] >= arr_times[iarr] + gap:
            iarr += 1
        elif arr_times[iarr] >= dep_times[idep]:
            idep += 1
    return False


def build_adjacent_routes_heuristic():
    adjacence = set()
    results = execute_select("SELECT node1, node2 FROM graph_nearby_nodes")
    log_info("Build adjacency routes for %d parirs of stops" % len(results))
    for i, item in enumerate(results):
        if i % 1000 == 0:
            log_info("%d - %d" % (i, len(adjacence)))
        node1, node2 = int(item[0]), int(item[1])
        result_routes1 = execute_select("SELECT arc_id, route FROM graph_arcs WHERE arr_node = %d" % node1)
        result_routes2 = execute_select("SELECT arc_id, route FROM graph_arcs WHERE dep_node = %d" % node2)
        for item1 in result_routes1:
            for item2 in result_routes2:
                _arc1, route1 = int(item1[0]), int(item1[1])
                _arc2, route2 = int(item2[0]), int(item2[1])
                if (route1, route2) not in adjacence and adjacent(get_arr_times(_arc1), get_dep_times(_arc2)):
                    adjacence.add((route1, route2))
    log_info("%d pairs of adjacent routes to be inserted" % len(adjacence))
    for route1, route2 in adjacence:
        inputs = [("route1", route1), ("route2", route2)]
        execute_insert("graph_adjacent_routes", inputs)
    execute_sql("commit")


if __name__ == "__main__":
    print("Problem?")
    build_adjacent_routes()

