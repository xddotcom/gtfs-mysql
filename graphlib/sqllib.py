'''
Created on 2012-7-15

@author: DXD.Spirits
'''

graph_table_list = {'graph_shortcuts', 'graph_nearby_nodes', 'graph_adjacent_routes', 
                    'graph_nodes', 'graph_arcs', 'graph_timetable', 'graph_routes', 'graph_route_incidence'}
table_sql = dict()


""" ------------------------------ graph profiles ------------------------------ """


table_sql["graph_shortcuts"] = """
CREATE TABLE graph_shortcuts (
    dep_node    int  NOT NULL,
    arr_node    int  NOT NULL,
    cost        int  NOT NULL,
    KEY adjacence1 (dep_node, arr_node),
    KEY adjacence2 (arr_node, dep_node)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='shortcuts from/to hubs or between hubs';
"""


table_sql["graph_nearby_nodes"] = """
CREATE TABLE graph_nearby_nodes (
    node1      int  NOT NULL,
    node2      int  NOT NULL,
    cost       int  NOT NULL,
    KEY nodes1 (node1),
    KEY nodes2 (node2)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='nearby nodes on the engire graph';
"""


table_sql["graph_adjacent_routes"] = """
CREATE TABLE graph_adjacent_routes (
    route1     int  NOT NULL,
    route2     int  NOT NULL,
    KEY routes1 (route1),
    KEY routes2 (route2)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='route adjacence';
"""


""" ------------------------------ graph tables ------------------------------ """


table_sql["graph_nodes"] = """
CREATE TABLE graph_nodes (
    node_id      int          NOT NULL AUTO_INCREMENT,
    ishub        int          DEFAULT 0,
    latitude     int          NOT NULL,
    longitude    int          NOT NULL,
    tile_id      varchar(32)  NOT NULL,
    gtfs_dataset int          NOT NULL,
    gtfs_stop    varchar(50)  NOT NULL,
    KEY tiles (tile_id),
    KEY hubs (ishub),
    PRIMARY KEY (node_id)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='graph node map to gtfs stop, with hub level';
"""


table_sql["graph_arcs"] = """
CREATE TABLE graph_arcs (
    arc_id      int  NOT NULL AUTO_INCREMENT,
    dep_node    int  NOT NULL,
    arr_node    int  NOT NULL,
    route       int  NOT NULL,
    cost        int  NOT NULL,
    KEY adjacence1 (dep_node, arr_node),
    KEY adjacence2 (arr_node, dep_node),
    PRIMARY KEY (arc_id)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='stop times with graph route';
"""


table_sql["graph_timetable"] = """
CREATE TABLE graph_timetable (
    arc_id      int  NOT NULL,
    dep_time    int  NOT NULL,
    arr_time    int  NOT NULL,
    days        int  NOT NULL,
    KEY departures (arc_id, dep_time),
    KEY arrivals (arc_id, arr_time)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='stop times for the graph';
"""


table_sql["graph_routes"] = """
CREATE TABLE graph_routes (
    route_id      int          NOT NULL AUTO_INCREMENT,
    gtfs_dataset  int          NOT NULL,
    gtfs_route    varchar(50)  NOT NULL,
    KEY gtfs_routes (gtfs_dataset, gtfs_route),
    PRIMARY KEY (route_id)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='regroup all the trips sharing the same sequence of nodes, walking route_id == 0';
"""


table_sql["graph_route_incidence"] = """
CREATE TABLE graph_route_incidence (
    node_id      int   NOT NULL,
    route_id     int   NOT NULL,
    KEY nodes (node_id),
    KEY routes (route_id)
) ENGINE=%s DEFAULT CHARSET=utf8
COMMENT='route incidence';
"""

