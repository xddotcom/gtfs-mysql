'''
Created on Jul 17, 2012

@author: Xindong
'''

from shortcuts import execute_select, log_info
import os


str_body = """<?xml version="1.0" encoding="UTF-8"?>
<gexf xmlns="http://www.gexf.net/1.2draft" 
    xmlns:viz="http://www.gexf.net/1.2draft/viz" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:schemaLocation="http://www.gexf.net/1.2draft 
                        http://www.gexf.net/1.2draft/gexf.xsd" 
    version="1.2">
    <graph mode="dynamic" defaultedgetype="undirected" timeformat="double" start="0.0">
        <attributes class="node" mode="static">
            <attribute id="lat" title="latitude" type="double"/>
            <attribute id="lon" title="longitude" type="double"/>
            <attribute id="level" title="level" type="integer"/>
            <attribute id="dataset" title="dataset" type="integer"/>
        </attributes>
        <attributes class="edge" mode="static">
            <attribute id="level" title="level" type="integer"/>
        </attributes>
        <nodes>%s
        </nodes>
        <edges>%s
        </edges>
    </graph>
</gexf>"""


color = {}
############# R ### G ### B #
color[0] = (0xFF, 0xFF, 0x00)
color[1] = (0xFF, 0x00, 0x00)
color[2] = (0xFF, 0xA5, 0x00)
color[3] = (0x00, 0xFF, 0xFF)
color[4] = (0x00, 0xFF, 0x00)


size = {}
size[0] = 10
size[1] = 15
size[2] = 20
size[3] = 25
size[4] = 30


def node_gexf(node_id, latitude = 0, longitude = 0, level = 0, start = 0, end = 0):
    node_color = 'r="%d" g="%d" b="%d" a="1"' % color[level]
    node_size = size[level]
    dataset_id = node_id[0]
    return """
            <node id="%s" label="%s">
                <attvalues>
                    <attvalue for="lat" value="%f"/>
                    <attvalue for="lon" value="%f"/>
                    <attvalue for="level" value="%d"/>
                    <attvalue for="dataset" value="%d"/>
                </attvalues>
                <spells>
                    <spell start="%f" end="%f" />
                </spells>
                <viz:size value="%d"/>
                <viz:color %s />
            </node>""" % (node_id, node_id, 
                          latitude, longitude, level, dataset_id, 
                          start, end,
                          node_size, 
                          node_color)


def edge_gexf(arc_id, dep_node, arr_node, level = 0):
    edge_color = 'r="%d" g="%d" b="%d" a="1"' % color[level]
    arc_label = arc_id.split("@")[0]
    return """
            <edge id="%s" label="%s" source="%s" target="%s" weight="1">
                <attvalues>
                    <attvalue for="level" value="%d"/>
                </attvalues>
                <spells>
                    <spell start="0.0" />
                </spells>
                <viz:color %s />
            </edge>""" % (arc_id, arc_label, dep_node, arr_node, 
                          level, 
                          edge_color)


def graph_to_gexf(nodes, arcs, filename):
    ''' nodes
            key: node_id
            value: a dict of latitude, longitude, level, ...
        arcs
            key: (dep, arr
            value: a dict of arc_id, level, ...
    '''
    nodes_str = ""
    edges_str = ""
    
    for (node_id, node) in nodes.iteritems():
        nodes_str += node_gexf(node_id, node["latitude"], node["longitude"], 
                               node["level"], node["start"], node["end"])
    
    for ((dep_node, arr_node), arc) in arcs.iteritems():
        arc_id = arc["arc_id"]
        level = arc["level"]
        edges_str += edge_gexf(arc_id, dep_node, arr_node, level)
    
    graph_out = str_body % (nodes_str, edges_str)
    with open(os.environ['HOME'] + "\\output\\%s.gexf" % filename, 'w') as f:
        f.write(graph_out)
    log_info("Exported: " + filename)


def load_nodes(dataset_list):
    #log_info("Load node coordinates")
    nodes = dict()
    for dataset_name in dataset_list:
        results = execute_select("""
            SELECT dataset_id, node_id, latitude, longitude
            FROM %s_graph_nodes
        """ % dataset_name)
        for item in results:
            node_id = (int(item[0]), int(item[1]))
            latitude, longitude = int(item[2])/1e+5, int(item[3])/1e+5
            nodes[node_id] = {"latitude": latitude, "longitude": longitude, "level": 0, "start": 0, "end": 0}
    return nodes


def load_arcs(dataset_list):
    #log_info("Load original graphs")
    arcs = dict()
    for dataset_name in dataset_list:
        results = execute_select("""
            SELECT dataset_id, dep_node, arr_node, min(cost)
            FROM %s_graph_arcs
            GROUP BY dep_node, arr_node
        """ % dataset_name)
        for item in results:
            dep_node = (int(item[0]), int(item[1]))
            arr_node = (int(item[0]), int(item[2]))
            arc_id = "%s-%s" % (dep_node, arr_node)
            cost = int(item[3])
            arcs[(dep_node, arr_node)] = {"arc_id": arc_id, "level": 0, "cost": cost}
    return arcs


def output_martins_sequence(dataset_list, filename, node_sequence):
    ''' path: [(dataset_id, node_id, sub_node_id(arc)), ...]
    '''
    log_info("Export martins solutions to gexf ... ")
    nodes = load_nodes(dataset_list)
    arcs = load_arcs(dataset_list)
    node_start = {}
    seq = 0
    for x in node_sequence:
        if (x[0], x[1]) not in node_start:
            seq += 1
            node_start[(x[0], x[1])] = seq
    for x in nodes:
        node_start.setdefault(x, seq + 1)
    
    graph_to_gexf(nodes, arcs, node_start, filename)


def output_martins_solution(dataset_list, filename, paths, node_sequence):
    ''' path: [(dataset_id, node_id, sub_node_id(arc)), ...]
    '''
    log_info("Export martins solutions to gexf ... ")
    nodes = load_nodes(dataset_list)
    arcs = load_arcs(dataset_list)
    
    node_start = {}
    seq = 0
    for x in node_sequence:
        if (x[0], x[1]) not in node_start:
            seq += 1
            node_start[(x[0], x[1])] = seq
    final_seq = seq
    
    for i, path in enumerate(paths[:3]):
        for x, y in zip(path[:-1], path[1:]):
            dep_arc, arr_arc = (x[0], x[2]), (y[0], y[2])
            dep_pnode, arr_pnode = (x[0], x[1]), (y[0], y[1])
            dep_node, arr_node = (x[0], x[1], i + 1), (y[0], y[1], i + 1)
            nodes[dep_node] = {"latitude": nodes[dep_pnode]["latitude"], "longitude": nodes[dep_pnode]["longitude"], 
                               "level": i + 1, "start": final_seq + 1, "end": final_seq + 1}
            nodes[arr_node] = {"latitude": nodes[arr_pnode]["latitude"], "longitude": nodes[arr_pnode]["longitude"], 
                               "level": i + 1, "start": final_seq + 1, "end": final_seq + 1}
            if (dep_node != arr_node):
                if dep_arc == arr_arc:
                    item = execute_select("""
                        SELECT dataset_id, route_id FROM gtfs_graph_arcs 
                        WHERE dataset_id=%d AND arc_id=%d LIMIT 1
                    """ % dep_arc)[0]
                    route = (int(item[0]), int(item[1])) 
                else:
                    route = 'walk'
                arc_id = "%s@%s-%s" % (route, dep_node, arr_node)
                arcs[(dep_node, arr_node)] = {"arc_id": arc_id, "level": i + 1, "cost": 1}
    
    for x in node_sequence:
        node_visit = (x[0], x[1], "visit")
        pnode = (x[0], x[1])
        nodes[node_visit] = {"latitude": nodes[pnode]["latitude"], "longitude": nodes[pnode]["longitude"], 
                             "level": 4, "start": node_start[pnode], "end": final_seq}
    
    graph_to_gexf(nodes, arcs, filename)


if __name__ == "__main__":
    #dataset_list = ["nyctrs", "njtrs", "nynjpa", "mtrnth", "lirail"]
    #output_highway_graph(dataset_list, "graph")
    pass

