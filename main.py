'''
Created on Jun 29, 2012

@author: Xindong
'''

from shortcuts import execute_sql
import gtfsdb
import os


dataset_path = dict()

#dataset_path["test"]   = os.environ["HOME"] + '\\GTFS Feeds\\testwalk\\'

dataset_path["lirail"] = os.environ["HOME"] + '\\GTFS Feeds\\long-island-rail-road\\'
dataset_path["libus"]  = os.environ["HOME"] + '\\GTFS Feeds\\long-island-bus\\'
dataset_path["mtrnth"] = os.environ["HOME"] + '\\GTFS Feeds\\metro-north-archiver\\'
dataset_path["nycbus"] = os.environ["HOME"] + '\\GTFS Feeds\\mta-bus-company\\'
dataset_path["nyctrs"] = os.environ["HOME"] + '\\GTFS Feeds\\mta-new-york-city-transit\\'
dataset_path["nynjpa"] = os.environ["HOME"] + '\\GTFS Feeds\\port-authority-of-new-york-new-jersey\\'
dataset_path["njtrs"]  = os.environ["HOME"] + '\\GTFS Feeds\\mbmccormick\\'

#dataset_path["sncf"]  = os.environ["HOME"] + '\\GTFS Feeds\\export-TN-GTFS-LAST\\'

#dataset_path["lonbus"]  = os.environ["HOME"] + "\\GTFS data on ncepmelet\\data\\uk\\lon\\bus\\2012_08_07"
#dataset_path["londlr"]  = os.environ["HOME"] + "\\GTFS data on ncepmelet\\data\\uk\\lon\\DLR\\2012_08_07"
#dataset_path["lontrm"]  = os.environ["HOME"] + "\\GTFS data on ncepmelet\\data\\uk\\lon\\tramlink\\2012_08_07"
#dataset_path["lonmtr"]  = os.environ["HOME"] + "\\GTFS data on ncepmelet\\data\\uk\\lon\\underground\\2012_08_07"

#dataset_path["se"]  = os.environ["HOME"] + "\\GTFS data on ncepmelet\\data\\se\\2012_08_01"


def letsgo():
    gtfsdb.prepare_db()
    
    for dataset_name in dataset_path:
        gtfsdb.drop_dataset(dataset_name)
        gtfsdb.create_dataset(dataset_name)
        gtfsdb.import_data(dataset_name, dataset_path[dataset_name])
    
    gtfsdb.build_union()
    execute_sql("commit")


if __name__ == '__main__':
    letsgo()

